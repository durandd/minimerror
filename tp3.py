from array import array
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
from minimerror import Minimerror

def prepare_data():
    complete_dataset = np.genfromtxt('sonar.all-data', delimiter=',')

    boolean_list = list()

    with open('sonar.rocks', 'r') as rocks_file:
        content = rocks_file.readlines()

    line = 8
    while(line < len(content)):
        boolean_list.append(True) if content[line][0] == '*' else boolean_list.append(False)
        line += 12

    with open('sonar.mines', 'r') as rocks_file:
        content = rocks_file.readlines()

    line = 9
    while(line < len(content)):
        boolean_list.append(True) if content[line][0] == '*' else boolean_list.append(False)
        line += 12

    training_array = list()
    testing_array = list()
    for i in range(len(boolean_list)):
        if boolean_list[i]:
            testing_array.append(complete_dataset[i])
        else :
            training_array.append(complete_dataset[i])

    training_dataset = np.array(training_array)
    testing_dataset = np.array(testing_array)
    return complete_dataset, training_dataset, testing_dataset

def test_AND_dataset():
    # construct the AND dataset
    X = np.array([[-1, -1], [-1, 1], [1, -1], [1, 1]])
    y = np.array([[-1], [-1], [-1], [1]])
    # define our perceptron and train it
    print("[INFO] training perceptron...")
    p = Minimerror(N=X.shape[1])
    epochs = p.fit(X, y, epochs=100)
    # now that our perceptron is trained we can evaluate it
    print("[INFO] testing perceptron...")
    # now that our network is trained, loop over the data points
    for (x, target) in zip(X, y):
        # make a prediction on the data point and display the result
        # to our console
        pred = p.predict(x)
        print("[INFO] data={}, ground-truth={}, pred={}, epochs={}".format(
            x, target[0], pred, epochs))
    p.plot_stabilities("AND dataset")

def test_OR_dataset():
    # construct the OR dataset
    X = np.array([[-1, -1], [-1, 1], [1, -1], [1, 1]])
    y = np.array([[-1], [1], [1], [1]])

    # define our perceptron and train it
    print("[INFO] training perceptron...")
    p = Minimerror(N=X.shape[1])
    epochs = p.fit(X, y, epochs=100)

    # now that our perceptron is trained we can evaluate it
    print("[INFO] testing perceptron...")
    # now that our network is trained, loop over the data points
    for (x, target) in zip(X, y):
        # make a prediction on the data point and display the result
        # to our console
        pred = p.predict(x)
        print("[INFO] data={}, ground-truth={}, pred={}, epochs={}".format(
            x, target[0], pred, epochs))
    p.plot_stabilities("OR dataset")

def test_XOR_dataset():
    # construct the OR dataset
    X = np.array([[-1,-1], [-1,1], [1,-1], [1,1]])
    y = np.array([[-1], [1], [1], [-1]])
    # define our perceptron and train it
    print("[INFO] training perceptron...")
    p = Minimerror(N=X.shape[1])
    epochs = p.fit(X, y, epochs=100)


    # now that our perceptron is trained we can evaluate it
    print("[INFO] testing perceptron...")
    # now that our network is trained, loop over the data points
    for (x, target) in zip(X, y):
        # make a prediction on the data point and display the result
        # to our console
        pred = p.predict(x)
        print("[INFO] data={}, ground-truth={}, pred={}, epochs={}".format(
            x, target[0], pred, epochs))
    p.plot_stabilities("XOR dataset")
            

def train_and_test(training_dataset, testing_dataset):
    # train perceptron on training_dataset
    perceptron = Minimerror(60)
    perceptron.fit(training_dataset[:, :-1], training_dataset[:, -1], epochs=1000)
    # test perceptron on testing_dataset
    eg = 0
    for (x, target) in zip(training_dataset[:, :-1], testing_dataset[:, -1]):
        pred = perceptron.predict(x)
        if pred != target :
            eg += 1
    perceptron.plot_stabilities("Rocks and mines dataset")
    return perceptron.W, eg

if __name__ == "__main__":
    
    test_AND_dataset()
    test_OR_dataset()
    test_XOR_dataset()
    complete_dataset, training_dataset, testing_dataset = prepare_data()
    w, eg = train_and_test(training_dataset, testing_dataset)

    print('w = \n' + str(w))
    print('eg = ' + str(eg))

