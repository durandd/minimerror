import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt


class Minimerror:
    def __init__(self, N, recuit = 0.01 ):
        # initialize the weight matrix and store the learning rate
        self.W = np.random.randn(N + 1) / np.sqrt(N)
        self.recuit = recuit
        self.N = N

    def heaviside(self, x):
        # apply the heaviside function
        return 1 if x > 0 else -1

    def predict(self, X, addBias=True):
        # ensure our input is a matrix
        X = np.atleast_2d(X)
        if addBias:
            # insert a column of 1's as the last entry in the feature
            # matrix (bias)
            X = np.c_[X, np.ones((X.shape[0]))]
        # take the dot product between the input features and the
        # weight matrix, then pass the value through the heaviside
        # function
        return self.heaviside(np.dot(X, self.W))
    
    def fit(self, X, Y, epochs, beta = 0.1):        

        # add the bias
        X = np.c_[X, np.ones((X.shape[0]))]
        
        # Hebbian Learning Rule
        for (x, y) in zip(X, Y) :
            self.W += x * y
        self.W = self.W / LA.norm(self.W)

        # create list of stabilies over time for plot
        if not hasattr(self, 'stabilities'):
            self.stabilities = []
            for i in range(X.shape[0]):
                self.stabilities.append([])
        # loop over the desired number of epochs
        epoch = 0
        while epoch <= epochs:
            delta = 0
            w_norm  = LA.norm(self.W)
            for i, (x, y) in enumerate(zip(X, Y)):
                x = np.atleast_2d(x)
                
                # get stability
                stability = y * np.dot(x, self.W) / w_norm

                # save stability in a list for plot
                self.stabilities[i].append(stability)

                # derived of the loss function
                delta += x / (np.cosh(beta * stability / 2)**2) * y
            delta = - delta * beta / 4
            self.W -= delta[0]
            # la normalisation introduit un beug au bout de beaucoup d'epochs
            #self.W = self.W / LA.norm(self.W)

            # updtate beta each epoch
            beta += self.recuit

            # update the epoch
            epoch += 1
        return epoch - 1


    def plot_stabilities(self, name = None):
        if name :
            plt.figure(name)
        for stability_list in self.stabilities:
            plt.plot(stability_list)
            plt.title("Evolution of the stabilities over time")
        plt.show()
